/*
PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig"

u2ps: u2ps.c
	gcc -g -o $@ $< `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config pangocairo --cflags --libs`
 */

#include <cairo.h>
#include <cairo-pdf.h>
#include <cairo-ps.h>
#include <pango/pangocairo.h>

#include <locale.h>

#include <math.h>
#include <time.h>

#ifdef USE_NROFF
#include "goption.c"
#endif

typedef enum {
  PAPER_DEFINITION_END = -1,
  PAPER_A3,
  PAPER_A4,
  PAPER_B4,
  PAPER_B5,
  PAPER_LETTER,
} PaperType;

typedef enum {
  OUTPUT_PDF,
  OUTPUT_PS,
} OutputType;

struct paperlen {
  gdouble paper_long;
  gdouble paper_short;
};

typedef struct _PaperSize {
  PaperType type;
  struct paperlen size;
} PaperSize;

PaperSize paper_size[] = {
  /* PaperType, { Longer, Shorter }, */

  { PAPER_LETTER, {792, 612} },
  { PAPER_A4, {841.99, 594.35} },
  { PAPER_A3, {1190, 842} },
  { PAPER_B5, {729, 516} },
  { PAPER_B4, {1032, 729} },
  { PAPER_DEFINITION_END }
};

#define VERSION "0.8"

#define degree_to_radian(d) (((d)*M_PI/180.0))
#define DIV_SQRT2 (0.70710678118)
#define SQRT2 (1.41421356237)

/* Default A4 in points */
static gdouble pagew = 841.99;
static gdouble pageh = 594.35;

static gboolean option_pango_markup = FALSE;

static gdouble option_margin_top = 30;
static gdouble option_margin_left = 30;
static gdouble option_margin_right = 30;
static gdouble option_margin_bottom = 30;
static gdouble option_margin_center = 30;
static gdouble option_xoffset_center = 0;
static gdouble option_yoffset_center = 0;
static gdouble option_height_header = 20;
static gdouble option_pango_line_spacing = 0.0;
static gchar* option_pango_font_family = "MonoSpace";
static gchar* option_pango_wrap = NULL;
static PangoWrapMode pango_wrap = PANGO_WRAP_WORD_CHAR;
static gboolean option_portrait = FALSE;
static gchar *option_center_title = NULL;
static gchar *option_left_title = NULL;
static gchar *option_right_title = NULL;
static gboolean option_line_numbers = FALSE;
static gboolean option_faster = FALSE;
static gboolean option_nroff = FALSE;
static gchar* option_medium_type = NULL;
static gboolean option_version = FALSE;
static gchar* option_output = NULL;
static gchar* option_output_type = NULL;
static gboolean option_watermark = FALSE;
static gchar* option_watermark_text = NULL;
static gchar* option_watermark_font_family = NULL;

static PaperType medium_type = PAPER_A4;
static OutputType output_type = OUTPUT_PDF;

static gdouble height_header = 20;
static gdouble width_header;
static gdouble height_body;
static gdouble width_body;
static gdouble margin_body = 5;
static gdouble two_up_xoffset = 0;
static gdouble height_line_numbers;
static gdouble width_line_numbers;
static gint page_index = 0;
static gint line_index = 0;
static gchar datestr[1024];
static gchar *filename = NULL;
static gint total_pages = 1;
static GSList *line_slist = NULL;
static gint drew_size = 0;
static gchar *fcontent = NULL;
static gint fcontent_size = 0;
static GSList *page_wrapline_slist = NULL;
static gint wrap_line_offset = 0;
static PangoAttrList *attr_list = NULL;

/*
 * Line index start from 0
 *
 * page_head_offset: page head offset from file head
 * line_head_offset: line head offset from file head
 * line_tail_offset: line tail offset from file head
 * page_tail_offset: page head offset from file head
 */

static void
show_version ()
{
  g_print("%s version %s\n", g_get_prgname (), VERSION);
  g_print("Copyright (C) 2019 Yukihiro Nakai\n");
  exit (0);
}

static gint
get_head_offset_of_line (gint line_index)
{
  GSList *slist = g_slist_nth (line_slist, line_index);
  if (slist) {
    return GPOINTER_TO_INT (slist->data);
  }

  g_error ("get_head_offset_of_line: line_index exceed: %d.\n", line_index);

  return -1;
}

static gint
get_tail_offset_of_line (gint line_index)
{
  GSList *slist = g_slist_nth (line_slist, line_index + 1);
  if (slist) {
    return GPOINTER_TO_INT (slist->data) - 1;
  }

  if (line_index + 1 == g_slist_length (line_slist)) {
    return fcontent_size;
  }

  g_error ("get_tail_offset_of_line: line_index exceed: %d.\n", line_index);

  return -1;
}

static gint
get_line_from_offset (gint offset)
{
  GSList *cursor = line_slist;
  gint i = 0;

  while(cursor) {
    gint start_offset = GPOINTER_TO_INT(cursor->data);
    if (start_offset > offset) {
      //g_print("offset:%d ==> line:%d\n", offset, i-1);
      return i - 1;
    }
    cursor = cursor->next;
    i++;
  }

  //g_print("afterloop: offset:%d ==> line:%d\n", offset, i-1);
  return i - 1;
}

static void
calculate_parameters(gdouble w, gdouble h)
{
  if (option_portrait) {
    width_body = w - option_margin_left - option_margin_right;
  } else {
    width_body = (w - option_margin_left - option_margin_right - option_margin_center) / 2.0;
  }

  two_up_xoffset = w / 2.0;
  width_header = width_body;
  height_body = h - option_margin_top - option_margin_bottom - height_header;

  time_t now_t = time(NULL);
  strftime(datestr, 1000, "%b %e %Y %H:%M", localtime(&now_t));
}

static PangoLayout*
create_pango_layout (cairo_t *cr, gdouble font_size)
{
  PangoLayout *layout = pango_cairo_create_layout(cr);
  PangoContext *context = pango_layout_get_context(layout);
  PangoFontDescription *desc = pango_context_get_font_description(context);
  pango_font_description_set_family(desc, option_pango_font_family);
  pango_font_description_set_style(desc, PANGO_STYLE_NORMAL);
  pango_font_description_set_size(desc, font_size);
  pango_layout_context_changed (layout);

  return layout;
}

/*
 * Draw Watermark in frame
 */
void
draw_watermark(cairo_t *cr, gint xoffset) {
  if (!option_watermark) {
    return;
  }

  cairo_save (cr);
  cairo_select_font_face (cr, option_watermark_font_family,
    CAIRO_FONT_SLANT_NORMAL,
    CAIRO_FONT_WEIGHT_BOLD);
  gdouble font_size = 150.0;
  cairo_set_font_size (cr, font_size);

  cairo_text_extents_t text_extents;
  cairo_text_extents (cr, option_watermark_text, &text_extents);
  font_size = font_size * width_body * SQRT2 / (text_extents.width + text_extents.height);

  cairo_set_font_size (cr, font_size);
  cairo_text_extents (cr, option_watermark_text, &text_extents);

    /*
     * Offset from the frame top left:
     * x = text_extents.height * DIV_SQRT2
     * y = height_frame/2 + (ext_extents.width+text_extents.height)/2*DIV_SQRT2
     */

    gdouble wm_xoffset = option_margin_left + text_extents.height * DIV_SQRT2;
    gdouble wm_yoffset = option_margin_top + (height_body + height_header)/2.0 + (text_extents.width + text_extents.height)/2.0 * DIV_SQRT2;
    cairo_move_to (cr, wm_xoffset + xoffset, wm_yoffset);

    cairo_rotate (cr, degree_to_radian(-45));
    cairo_set_source_rgba (cr, 0.8, 0.8, 0.8, 0.5);
    cairo_show_text(cr, option_watermark_text);

    cairo_restore (cr);
}

/*
 * Draw frames
 */
void
draw_frame(cairo_t *cr, gint xoffset, PangoLayout *body_layout) {
  PangoLayout *layout;

  if (line_index >= g_slist_length (line_slist)) {
    return;
  }

  /*
   * Draw Frame
   */
  cairo_move_to(cr, option_margin_left + xoffset, option_margin_top);
  cairo_set_line_width(cr, 1);

  cairo_rectangle(cr, option_margin_left + xoffset, option_margin_top, width_header, height_header);
  cairo_set_source_rgb(cr, 0.95, 0.95, 0.95);
  cairo_fill(cr);
  cairo_set_source_rgb(cr, 0, 0, 0);
  cairo_rectangle(cr, option_margin_left + xoffset, option_margin_top, width_header, height_header);
  cairo_rectangle(cr, option_margin_left + xoffset, option_margin_top + height_header, width_body, height_body);
  cairo_stroke(cr);

  /*
   * Calculate header layout height
   */
  layout = create_pango_layout(cr, 10000);
  PangoRectangle ink_rect,logical_rect;
  pango_layout_get_pixel_extents (layout, &ink_rect, &logical_rect);
  gdouble header_yoffset = height_header - logical_rect.height;

  /*
   * Date header
   */
  cairo_move_to(cr, option_margin_left + xoffset + margin_body, option_margin_top + header_yoffset / 2);
  layout = create_pango_layout(cr, 10000);
  pango_layout_set_width(layout, (width_header - margin_body * 2) * PANGO_SCALE);
  pango_layout_set_height(layout, height_header * PANGO_SCALE);
  pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
  if (option_left_title) {
    pango_layout_set_text(layout, option_left_title, -1);
  } else {
    pango_layout_set_text(layout, datestr, -1);
  }
  pango_cairo_show_layout(cr, layout);
  g_object_unref (layout);

  /*
   * Filename at top center
   */
  layout = create_pango_layout(cr, 10000);
  if (option_center_title) {
    pango_layout_set_text(layout, option_center_title, -1);
  } else {
    gchar* basename = g_path_get_basename (filename);
    pango_layout_set_text(layout, basename, -1);
    g_free (basename);
  }
  pango_layout_set_width(layout, (width_header - margin_body * 2) * PANGO_SCALE);
  pango_layout_set_height(layout, height_header * PANGO_SCALE);
  pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
  pango_cairo_show_layout(cr, layout);
  g_object_unref (layout);
  
  /*
   * Page at top right
   */
  layout = create_pango_layout(cr, 10000);
  if (option_right_title) {
    pango_layout_set_text(layout, option_right_title, -1);
  } else {
    gchar* page_text = g_strdup_printf("Page %d/%d", page_index + 1, total_pages);
    pango_layout_set_text(layout, page_text, -1);
    g_free (page_text);
  }
  pango_layout_set_width(layout, (width_header - margin_body * 2) * PANGO_SCALE);
  pango_layout_set_height(layout, height_header * PANGO_SCALE);
  pango_layout_set_alignment(layout, PANGO_ALIGN_RIGHT);
  pango_cairo_show_layout(cr, layout);
  g_object_unref (layout);

  /*
   * Draw Body
   */
  cairo_move_to(cr, option_margin_left + xoffset + margin_body + width_line_numbers, option_margin_top + height_header);

  pango_cairo_show_layout (cr, body_layout);

  gint layout_lines = pango_layout_get_line_count (body_layout);

  /*
   * Show line numbers
   */
  if (option_line_numbers) {
    cairo_move_to(cr, option_margin_left + xoffset + margin_body, option_margin_top + height_header);
    cairo_select_font_face (cr, option_pango_font_family, CAIRO_FONT_SLANT_NORMAL,
        CAIRO_FONT_WEIGHT_NORMAL);
    cairo_set_font_size (cr, 6144/PANGO_SCALE);

    PangoLayoutIter *iter = pango_layout_get_iter (body_layout);
    gint page_line_index = 0;
    gint pango_line_index = 0;
    gint page_head_offset = (gint)get_head_offset_of_line (line_index);
    GSList *wrap_slist = g_slist_nth_data (page_wrapline_slist, page_index);

    GSList *lines = pango_layout_get_lines (body_layout);

    while (lines) {
      PangoLayoutLine *line = (PangoLayoutLine*)lines->data;
      page_line_index = get_line_from_offset (page_head_offset + line->start_index) - line_index;

      gint wrap_offset = (int)GPOINTER_TO_INT(g_slist_nth_data (wrap_slist, pango_line_index));
      //g_print ("line:%d wrapped:%d\n", line_index + pango_line_index, wrap_offset);

      if (wrap_offset && pango_line_index) {
        pango_line_index++;
        lines = lines->next;
        pango_layout_iter_next_line (iter);
        continue;
      }
      if (wrap_offset && pango_line_index == 0) {
        page_head_offset = wrap_offset;
        page_line_index = get_line_from_offset (wrap_offset) - line_index;
      }

//g_print ("number: %d line:%d, page:%d pango:%d page_head_offset:%d lineoffset:%d length:%d\n", line_index + page_line_index + 1, line_index, page_line_index, pango_line_index, page_head_offset, line->start_index, line->length);

      gdouble iter_baseline = (gdouble)pango_layout_iter_get_baseline (iter) / PANGO_SCALE;
      cairo_move_to(cr, option_margin_left + xoffset + margin_body + logical_rect.x, option_margin_top + height_header + iter_baseline);
      cairo_text_extents_t text_extents, text_extents_full;
      gchar* line_text = g_strdup_printf("%3d:", line_index + page_line_index + 1);
      cairo_text_extents (cr, line_text, &text_extents);
      cairo_text_extents (cr, "000:", &text_extents_full);
      cairo_rel_move_to (cr, text_extents_full.x_advance - text_extents.x_advance, 0);
      cairo_show_text (cr, line_text);

      /* End of loop */
      pango_line_index++;
      lines = lines->next;
      pango_layout_iter_next_line (iter);
    }

//    g_print ("layout_lines: %d\n", layout_lines);
    line_index += page_line_index + 1;
//    g_print ("line_index: %d\n", line_index);

  }

  page_index++;

  return;
} /* End of drawframe */

int main(gint argc, gchar *argv[]) 
{
  PangoLayout *layout;
  PangoFontDescription *desc;
  PangoContext *context;

  GError *error = NULL;
  GOptionContext *option_context;

  GOptionEntry main_entries[] = {
    { "medium", 'M', 0,
      G_OPTION_ARG_STRING, &option_medium_type,
      "Paper medium type: A4/A3/B4/B5/Letter", "MEDIUM" },
    { "portrait", 'p', 0,
      G_OPTION_ARG_NONE, &option_portrait,
      "Portrait", NULL },
    { "center-title", 0, 0,
      G_OPTION_ARG_STRING, &option_center_title,
      "Center title in the header", "TITLE" },
    { "left-title", 0, 0,
      G_OPTION_ARG_STRING, &option_left_title,
      "Left title in the header", "TITLE" },
    { "right-title", 0, 0,
      G_OPTION_ARG_STRING, &option_right_title,
      "Right title in the header", "TITLE" },
    { "line-numbers", 'N', 0,
      G_OPTION_ARG_NONE, &option_line_numbers,
      "Display line numbers", NULL },
    { "faster", 0, 0,
      G_OPTION_ARG_NONE, &option_faster,
      "Cut features and then faster", NULL },
#ifdef _GOPTION_C
    { "nroff", 0, 0,
      G_OPTION_ARG_NONE, &option_nroff,
      "Generate nroff template of u2ps/u2pdf", NULL },
#endif
    { "output", 'o', 0,
      G_OPTION_ARG_STRING, &option_output,
      "Output filename", "FILENAME" },
    { "type", 't', 0,
      G_OPTION_ARG_STRING, &option_output_type,
      "Output type: ps, pdf", "TYPE" },
    { "watermark", 0, 0,
      G_OPTION_ARG_NONE, &option_watermark,
      "Add pseudo watermark", NULL },
    { "watermark-text", 0, 0,
      G_OPTION_ARG_STRING, &option_watermark_text,
      "Pseudo watermark text (Default:DRAFT)", NULL },
    { "watermark-font", 0, 0,
      G_OPTION_ARG_STRING, &option_watermark_font_family,
      "Pseudo watermark font", NULL },
    { NULL }
  };

  GOptionEntry pango_entries[] = {
    { "markup", 0, 0,
      G_OPTION_ARG_NONE, &option_pango_markup,
      "Parse input as pango markup", NULL },
    { "font-family", 0, 0,
      G_OPTION_ARG_STRING, &option_pango_font_family,
      "Font Family", "FONTFACE" },
    { "wrap", 0, 0,
      G_OPTION_ARG_STRING, &option_pango_wrap,
      "Wrap mode - word, char, wordchar", "WRAPMODE" },
    { NULL }
  };

  setlocale (LC_ALL, "");

  option_context = g_option_context_new ("- [text file]");
  g_option_context_add_main_entries (option_context, main_entries, NULL);
  g_option_context_set_summary (option_context, "u2ps/u2pdf - GNU a2ps style filter for UTF-8 text");
  g_option_context_set_description (option_context, "u2ps/u2pdf accept UTF-8 text and format to a2ps style PostScript or PDF.");

  GOptionGroup *pango_group;
  pango_group = g_option_group_new ("pango", "Pango Options:", "pango option group",
                0, NULL);
  g_option_group_add_entries (pango_group, pango_entries);
  g_option_context_add_group(option_context, pango_group);

  /*
   * Parse options
   */
  if (!g_option_context_parse (option_context, &argc, &argv, &error)) {
    g_print ("Failed to parse options: %s\n", error->message);
    g_error_free (error);
    return 1;
  }

  if (option_version) {
    show_version ();
  }

#ifdef _GOPTION_C
  if (option_nroff) {
    g_option_context_to_nroff (option_context);

    exit(0);
  }
#endif

  if (option_pango_wrap) {
    if (!g_ascii_strcasecmp (option_pango_wrap, "char")) {
      pango_wrap = PANGO_WRAP_CHAR;
    } else if (!g_ascii_strcasecmp (option_pango_wrap, "c")) {
      pango_wrap = PANGO_WRAP_CHAR;
    } else if (!g_ascii_strcasecmp (option_pango_wrap, "word")) {
      pango_wrap = PANGO_WRAP_WORD;
    } else if (!g_ascii_strcasecmp (option_pango_wrap, "w")) {
      pango_wrap = PANGO_WRAP_WORD;
    } else if (!g_ascii_strcasecmp (option_pango_wrap, "wordchar")) {
      pango_wrap = PANGO_WRAP_WORD_CHAR;
    } else if (!g_ascii_strcasecmp (option_pango_wrap, "wc")) {
      pango_wrap = PANGO_WRAP_WORD_CHAR;
    } else {
      g_warning ("Incorrect word wrap option: %s\n", option_pango_wrap);
    }
  }

  if (option_medium_type) {
    if (!g_ascii_strcasecmp (option_medium_type, "a4")) {
      medium_type = PAPER_A4;
    } else if (!g_ascii_strcasecmp (option_medium_type, "a3")) {
      medium_type = PAPER_A3;
    } else if (!g_ascii_strcasecmp (option_medium_type, "b5")) {
      medium_type = PAPER_B5;
    } else if (!g_ascii_strcasecmp (option_medium_type, "b4")) {
      medium_type = PAPER_B4;
    } else if (!g_ascii_strcasecmp (option_medium_type, "letter")) {
      medium_type = PAPER_LETTER;
    } else {
      g_warning ("Incorrect medium type option: %s\n", option_medium_type);
    }

    gint i = 0;
    while (paper_size[i].type != PAPER_DEFINITION_END) {
      if (paper_size[i].type == medium_type) {
        pagew = paper_size[i].size.paper_long;
        pageh = paper_size[i].size.paper_short;
        break;
      }
      i++;
    }
  }

  g_warn_if_fail (pagew >= pageh);

  if (option_portrait) {
    gdouble size_temp = pagew;
    pagew = pageh;
    pageh = size_temp;
  }

  const gchar* prgname = g_get_prgname ();

  /*
   * Set output type - ps/pdf
   *
   * Priority:
   * 1. -t(--type) option
   * 2. -o(--output) filename sufix
   * 3. command name u2ps/u2pdf
   */

  if (!g_strcmp0 (prgname, "u2ps")) {
    output_type = OUTPUT_PS;
  } else if (!g_strcmp0 (prgname, "u2pdf")) {
    output_type = OUTPUT_PDF;
  }

  if (option_output_type) {
    if (!g_ascii_strcasecmp(option_output_type, "ps")) {
      output_type = OUTPUT_PS;
    } else if (!g_ascii_strcasecmp(option_output_type, "pdf")) {
      output_type = OUTPUT_PDF;
    } else {
      g_warning ("Incorrect output type option: %s\n", option_output_type);
      g_free (option_output_type);
      option_output_type = NULL;
    }

  }

  if (argc >= 2 && argv[1] != NULL) {
    filename = argv[1];
  } else {
    g_error ("Need argument\n");
  }

  if (option_output) {
    if (strlen (option_output) == 0) {
      g_error ("Incorrect output option: \"%s\"\n", option_output);
    }

    gchar* suffix = g_strrstr (option_output, ".");

    if (suffix) {
      if (!option_output_type) {
        if (!g_ascii_strcasecmp (suffix, ".ps")) {
          output_type = OUTPUT_PS;
        } else if (!g_ascii_strcasecmp (suffix, ".pdf")) {
          output_type = OUTPUT_PDF;
        } else {
          output_type = OUTPUT_PDF;
        }
      }
    }
  } else {
    gchar* suffix = g_strrstr (filename, ".");
    if (suffix) {
      gchar* basename = g_strndup (filename, suffix - filename);
      switch (output_type) {
      case OUTPUT_PS:
        option_output = g_strdup_printf ("%s%s", basename, ".ps");
        break;
      case OUTPUT_PDF:
      default:
        option_output = g_strdup_printf ("%s%s", basename, ".pdf");
        break;
      }
    } else {
      switch (output_type) {
      case OUTPUT_PS:
        option_output = g_strdup_printf("%s.ps", filename);
        break;
      case OUTPUT_PDF:
      default:
        option_output = g_strdup_printf("%s.pdf", filename);
        break;
      }
    }
  }

  g_return_val_if_fail (g_strcmp0(filename, option_output) != 0, 1);

  cairo_surface_t *surface;
  cairo_t *cr;

  gsize filesize = 0;
  g_file_get_contents (filename, &fcontent, &filesize, &error);

  if (error) {
    g_error("Cannot open file:%s\n", error->message);
  }

  fcontent_size = (gint)filesize;

  if (option_pango_markup){
    char *text = NULL;
    gboolean ret = pango_parse_markup (fcontent, -1, 0, &attr_list, &text, NULL, &error);

    if (attr_list == NULL) {
      /* No markup detected */
      option_pango_markup = FALSE;
    }

    if (error) {
      g_error("ERROR: %s\n", error->message);
    }

    if (!ret) {
      g_error("ERROR: pango_parse_markup failed\n");
    }

    if (text) {
      g_free (fcontent);
      fcontent = text;
      fcontent_size = (gint)strlen (text);
    }
  }


  /*
   * Analyze text line offsets
   */
  clock_t start = clock ();

  line_slist = NULL;
  line_slist = g_slist_append (line_slist, GINT_TO_POINTER (0));
  gchar* cursor = fcontent;
  while (*cursor != '\0') {
    if (*cursor++ == '\n') {
      if (*cursor != '\0') {
        line_slist = g_slist_append (line_slist, GINT_TO_POINTER (cursor - fcontent));
      } else {
        break;
      }
    }
  }

  if (0) {
    GSList *cursor_slist = line_slist;
    gint i = 1;
    while(cursor_slist) {
      gint offset = GPOINTER_TO_INT (cursor_slist->data);
      g_print("line:%d => offset:%d\n", i++, offset);
      cursor_slist = cursor_slist->next;
    }
  }

  //g_print ("Lines: %d\n", g_slist_length (line_slist));
  //g_print ("Line break: %.10f sec\n", (double)(clock()-start)/CLOCKS_PER_SEC);

  start = clock();
  if (output_type == OUTPUT_PS) {
    surface = cairo_ps_surface_create(option_output, pagew, pageh);
  } else {
    surface = cairo_pdf_surface_create(option_output, pagew, pageh);
  }
  cr = cairo_create(surface);
  //g_print ("Create Surface: %.10f sec\n", (double)(clock()-start)/CLOCKS_PER_SEC);

  cairo_set_source_rgb(cr, 0, 0, 0);

  calculate_parameters(pagew, pageh);

  /*
   * Watermark
   */
  option_watermark = option_watermark | (option_watermark_text != NULL);

  if (option_watermark) {
    if (option_watermark_text == NULL) {
      option_watermark_text = "DRAFT";
    }

    if (option_watermark_font_family == NULL) {
      option_watermark_font_family = option_pango_font_family;
    }
  }
  
  /*
   * Split text to all pages
   */
  gint page_head_offset = 0;
  gint  file_line_index = 0;
  GSList *line_slist_cursor = line_slist;
  GSList *body_layout_slist = NULL;

  gint debug_page_index = 0;

  /*
   * Estimate 1
   * Estimate lines in a page (optimistic, larger is better)
   */
  PangoLayout *estimate_layout = create_pango_layout (cr, 6144);
  pango_layout_set_width(estimate_layout, (width_header - margin_body * 2 - width_line_numbers) * PANGO_SCALE);
  pango_layout_set_height(estimate_layout, height_body * PANGO_SCALE);
  pango_layout_set_text (estimate_layout, "000:", -1);
  PangoRectangle ink_rect, logical_rect;
  pango_layout_get_pixel_extents (estimate_layout, &ink_rect, &logical_rect);
  gint estimated_lines = (gint)floor(height_body / ink_rect.height);
  //g_print ("estimated lines: %d\n", estimated_lines);

  g_warn_if_fail (estimated_lines > 0);

  if (option_line_numbers) {
    height_line_numbers = height_body;
    width_line_numbers = logical_rect.width;
  } else {
    width_line_numbers = 0;
  }

  pango_layout_set_width(estimate_layout, (width_header - margin_body * 2 - width_line_numbers) * PANGO_SCALE);
  pango_layout_set_height(estimate_layout, height_body * PANGO_SCALE);
  pango_layout_set_wrap (estimate_layout, pango_wrap);

  /*
   *  Estimate 2
   *  Put estimated lines of text that never wraps
   */
  gint estimated_size = sizeof(gchar) * 2 * estimated_lines + 1;
  gchar *text = g_new0(gchar, estimated_size);
  gint i = 0;
  for (i = 0; i < estimated_lines; i++) {
    text[i*2] = 'A';
    text[i*2+1] = '\n';
  }

  pango_layout_set_text (estimate_layout, text, -1);
  estimated_lines = pango_layout_get_line_count (estimate_layout);
  //g_print ("Estimate 2: %d\n", estimated_lines);

  /* Use as template */
  pango_layout_set_text (estimate_layout, "", -1);

  while (line_slist_cursor) {
    PangoLayout *body_layout = pango_layout_copy (estimate_layout);
    body_layout_slist = g_slist_append (body_layout_slist, body_layout);

    /*
     * Get estimated lines of text
     */
    page_head_offset = (gint)GPOINTER_TO_INT (line_slist_cursor->data);

    if (wrap_line_offset) {
      //g_print("Rewind: %d => %d\n", page_head_offset, wrap_line_offset);
      page_head_offset = wrap_line_offset;
      wrap_line_offset = 0;
    }

    GSList *line_slist_end = g_slist_nth (line_slist_cursor, estimated_lines);
    gint page_text_length = -1;
    if (line_slist_end) {
      page_text_length = GPOINTER_TO_INT (line_slist_end->data) - page_head_offset;
    }

    pango_layout_set_text (body_layout, fcontent + page_head_offset, (gint)page_text_length);

    if (option_pango_markup) {
      if (page_head_offset == 0 ) {
        pango_layout_set_attributes (body_layout, attr_list);
      } else {
        PangoAttrList *new_attr_list = pango_attr_list_new ();
        GSList* attr_slist = pango_attr_list_get_attributes (attr_list);
        while(attr_slist) {
          PangoAttribute *attr = attr_slist->data;
          if (attr->start_index >= page_head_offset) {
            PangoAttribute *new_attr = pango_attribute_copy (attr);
            new_attr->start_index -= page_head_offset;
            new_attr->end_index -= page_head_offset;
            pango_attr_list_insert (new_attr_list, new_attr);
          }
          attr_slist = attr_slist->next;
        }
        pango_layout_set_attributes (body_layout, new_attr_list);
        pango_attr_list_unref (new_attr_list);
      }
    }

    //g_print ("text: %s\n", g_strndup(fcontent + page_head_offset, 50));

    GSList* slist = pango_layout_get_lines_readonly (body_layout);

    GSList* wrap_slist = NULL;
    gint pango_line_index = 0;
    gint page_line_index = file_line_index;

    while (slist) {
      if (page_line_index == g_slist_length (line_slist)) {
        /*
         * Pango appends a blank line at tail automatically.
         */
        //g_print("Last line done.\n");
        break;
      }

      gint line_head_offset = (gint)get_head_offset_of_line (page_line_index);
      PangoLayoutLine *line = (PangoLayoutLine*)(slist->data);

      /*
       * Wrap exceeded the end of the page
       */
      if (pango_line_index >= estimated_lines) {
        //g_print ("Wrap exceed the page\n");
        wrap_line_offset = page_head_offset + line->start_index;
        //g_print ("wrap_line_offset:%d\n", wrap_line_offset);
        break;
      }

//g_print("pango line index: %d\n", pango_line_index);

      gint wrap_offset = 0;;
      if (line_head_offset == page_head_offset + line->start_index) {
        //g_print ("no wrap: %d\n", pango_line_index);
        page_line_index++;
      } else {
        wrap_offset = page_head_offset + line->start_index;
        //g_print ("Wrap:%d orig:\"%d\" line:\"%d\" wrap_offset:%d\n", pango_line_index, line_head_offset, page_head_offset + line->start_index, wrap_offset);
      }
      wrap_slist = g_slist_append (wrap_slist, GINT_TO_POINTER(wrap_offset));

      slist = slist->next;
      pango_line_index++;
    }

    //g_print ("wrap_slist: %d\n", g_slist_length (wrap_slist));
    page_wrapline_slist = g_slist_append (page_wrapline_slist, wrap_slist);
    wrap_slist = NULL;

    /*
     * Set final text
     */
    gint page_tail_offset = (gint)get_tail_offset_of_line (page_line_index-1);

    if (wrap_line_offset) {
      page_tail_offset = wrap_line_offset - 1;
    }

    pango_layout_set_text (body_layout, fcontent + page_head_offset, page_tail_offset - page_head_offset);

    file_line_index = page_line_index;
    line_slist_cursor = g_slist_nth (line_slist, file_line_index);
    //g_print ("Page end:%d\n", page_line_index-1);
    //g_print ("Next start from:%d\n", file_line_index);
    //g_print ("Remained lines: %d\n", g_slist_length (line_slist_cursor));
    //g_print ("NEXT PAGE\n");
  }

  g_object_unref (estimate_layout);

  /*
   * Core part of drawing
   */
  total_pages = g_slist_length (body_layout_slist);

  GSList* body_layout_slist_cursor = body_layout_slist;
  gint xoffset = 0;
  while(body_layout_slist_cursor) {
    PangoLayout *body_layout = (PangoLayout*)(body_layout_slist_cursor->data);

    draw_watermark(cr, xoffset);
    draw_frame(cr, xoffset, body_layout);

    if (option_portrait || xoffset != 0) {
      cairo_show_page(cr);
    }

    body_layout_slist_cursor = g_slist_next (body_layout_slist_cursor);
    xoffset = (xoffset == 0 && !option_portrait) ? two_up_xoffset : 0;
  }

  if (xoffset != 0) {
    cairo_show_page(cr);
  }

  /*
   * Finalize
   */

  if (option_pango_markup) {
    pango_attr_list_unref (attr_list);
  }

  g_slist_free_full (body_layout_slist, g_object_unref);

  cairo_surface_destroy(surface);
  cairo_destroy(cr);

  return 0;
}
