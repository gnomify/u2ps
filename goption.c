#ifndef _GOPTION_C
#define _GOPTION_C
#endif

#include <glib.h>

/*
 * groff -man -Tascii u2ps.1 | less
 */

struct _GOptionContext
{
  GList           *groups;

  gchar           *parameter_string;
  gchar           *summary;
  gchar           *description;

  GTranslateFunc   translate_func;
  GDestroyNotify   translate_notify;
  gpointer         translate_data;

  guint            help_enabled   : 1;
  guint            ignore_unknown : 1;
  guint            strv_mode      : 1;
  guint            strict_posix   : 1;

  GOptionGroup    *main_group;

  /* We keep a list of change so we can revert them */
  GList           *changes;

  /* We also keep track of all argv elements
   * that should be NULLed or modified.
   */
  GList           *pending_nulls;
};

struct _GOptionGroup
{
  gchar           *name;
  gchar           *description;
  gchar           *help_description;

  gint             ref_count;

  GDestroyNotify   destroy_notify;
  gpointer         user_data;

  GTranslateFunc   translate_func;
  GDestroyNotify   translate_notify;
  gpointer         translate_data;

  GOptionEntry    *entries;
  gint             n_entries;

  GOptionParseFunc pre_parse_func;
  GOptionParseFunc post_parse_func;
  GOptionErrorFunc error_func;
};

/** COPY END **/

static void
dump_option_group (GOptionGroup *group)
{
  gint i = 0;

  for (i = 0; i < group->n_entries; i++) {
    GOptionEntry *entry = group->entries + i;
    gchar* short_option = "";
    gchar* long_option = "";
    gchar* arg_description = "";

    if (entry->short_name) {
      short_option = g_strdup_printf("\\fB-%c\\fR, ", entry->short_name);
    }

    long_option = g_strdup_printf("\\fB--%s\\fR", entry->long_name);

    if (entry->arg_description) {
      arg_description = g_strdup_printf("=\\fI%s\\fR", entry->arg_description);
    }

    g_print (".IP \"%s%s%s\"\n", short_option, long_option, arg_description);
    g_print("%s\n", entry->description);
  }
}

void
g_option_context_to_nroff (GOptionContext *context)
{
  const gchar* summary = g_option_context_get_summary (context);
  const gchar* description = g_option_context_get_description (context);

  g_print(".TH AUTO GENERATED MAN PAGE TEMPLATE from GOptionContext\n");
  g_print(".SH NAME\n");
  g_print("%s\n", summary);
  g_print(".SH SYNOPSYS\n");
  g_print(".B %s [OPTION...] %s\n", g_get_prgname (), context->parameter_string);
  g_print(".SH DESCRIPTION\n");
  g_print("%s\n", description);
  g_print(".SH OPTIONS (APPLICATION)\n");

  dump_option_group (context->main_group);

  if (context->groups) {
    GList *list = context->groups;

    while(list) {
      GOptionGroup *group = list->data;

      g_print(".SH OPTIONS (%s)\n", g_ascii_strup (group->name, -1));
      dump_option_group (group);
      
      list = list->next;
    }
  }
  g_print(".SH NOTICES\n");
  g_print("Word wrap logic is not perfect because Pango does not show wrap by line.\n");
  g_print(".TP\n");
  g_print("When the font you specify with \\fB--font-family\\fR doesn't support specific styles, fontconfig automatically use other fonts.\n");
  g_print(".TP\n");
  g_print("\\fB--nroff\\fR option uses the private fields of GOptionContext/GOptionGroup. It will crash when the upstream glib changed them.\n");
  g_print("\n\n\n");
}
