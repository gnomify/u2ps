PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig"

u2ps: u2ps.c goption.c
	gcc -g -o $@ $< `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config pangocairo --cflags --libs` -DUSE_NROFF
	ln -f u2ps u2pdf

macosx: u2ps.c fontfile.m
	gcc -g -c u2ps.c `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config pangocairo --cflags ` -DUSE_NROFF
	clang -c fontfile.m  `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config pangocairo --cflags`
	gcc -g -o u2pdf u2ps.o fontfile.o `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config pangocairo --cflags --libs` -framework Foundation -framework CoreGraphics

pangocairo-gtk: pangocairo-gtk.c
	gcc -g -o $@ $< `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config pangocairo gtk+-3.0 --cflags --libs`

cairo: cairo.c
	gcc -o $@ $< `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config cairo --cflags --libs`

cairosvg: cairosvg.c
	gcc -o $@ $< `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config cairo --cflags --libs` `pkg-config librsvg-2.0 --cflags --libs`

touch: touch.c
	gcc -o $@ $< `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config gtk+-3.0 --cflags --libs`

install: gfilteredit
	install -m 755 $< /usr/local/bin/
