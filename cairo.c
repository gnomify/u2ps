#include <cairo.h>
#include <cairo-pdf.h>

int main(void) 
{
  cairo_surface_t *surface;
  cairo_t *cr;

  surface = cairo_pdf_surface_create("pdffile.pdf", 841.99, 594.35);
  cr = cairo_create(surface);

  cairo_set_source_rgb(cr, 0, 0, 0);
  cairo_select_font_face (cr, "Osaka", CAIRO_FONT_SLANT_NORMAL,
      CAIRO_FONT_WEIGHT_NORMAL);
  cairo_set_font_size (cr, 40.0);

  cairo_move_to(cr, 10.0, 50.0);
  cairo_show_text(cr, "日本語UTF-8");

  cairo_show_page(cr);

  cairo_move_to(cr, 10.0, 50.0);
  cairo_show_text(cr, "日本語UTF-8");

  cairo_surface_destroy(surface);
  cairo_destroy(cr);

  return 0;
}
