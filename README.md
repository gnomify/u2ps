# u2ps/u2pdf

Revised version of gnome-u2ps:  
http://bonobo.gnome.gr.jp/~nakai/u2ps/  
https://github.com/avsej/gnome-u2ps

# Output sample

[output.pdf](https://gitlab.com/gnomify/u2ps/blob/master/output.pdf)  

# SYNTAX HIGHLIGHTING

u2ps/u2pdf recognize  Pango mark-upped text. with --markup option.  

[pango-syntax-highlighter: https://gitlab.com/gnomify/pango-syntax-highlighter](https://gitlab.com/gnomify/pango-syntax-highlighter)  
Extend the original to handle whole pygments 500+ supported languages.  

[Highlight: http://www.andre-simon.de/doku/highlight/en/highlight.php](http://www.andre-simon.de/doku/highlight/en/highlight.php )   
'Highlight' supports Pango markup output.  
highlight --out-format=pango u2ps.c

[ansifilter https://github.com/andre-simon/ansifilter](https://github.com/andre-simon/ansifilter)  
ansifilter converts ANSI color to pango markup.  
This makes the other syntax highlighters a preprocessor of u2ps/u2pdf even if they doesn't support Pango markup directly.   
  
[GNU source-highlight https://www.gnu.org/software/src-highlite/](https://www.gnu.org/software/src-highlite/)  
GNU source-highlight doesn't support pango markup ouput, but does ANSI color.  
  
[Ruby Rouge](https://github.com/rouge-ruby/rouge)  
Ruby Rouge supports ANSI color.

MISC  
Combinations of those filters would extend the world.  
html2pango https://gitlab.gnome.org/World/html2pango  
https://github.com/tzemanovic/highlight.js-cli  

# WATERMARK

u2ps/u2pdf can show watermark, it is not that of in PDF specification but pseudo one.
There is a way to support the real PDF watermark but I don't mention here.